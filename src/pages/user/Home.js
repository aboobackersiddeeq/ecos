import React from 'react';
import HeaderTwo from '../../components/header/HeaderTwo';
import '../../style/home.css';

const Home = () => {
  return (
    <>
      <HeaderTwo />
      <div>
        <img className="nexonEv" src="../../../Images/nexonEv.jpg" alt="" />
      </div>
    </>
  );
};

export default Home;
