import React from 'react';
import HeaderTwo from '../../../components/header/HeaderTwo';

const DarkEdition = () => {
  return (
    <>
      <HeaderTwo />
      <div>
        <img className="nexonEv" src="../../../Images/daskbanner.jpg" alt="" />
      </div>
    </>
  );
};

export default DarkEdition;
