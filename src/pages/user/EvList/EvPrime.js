import React from 'react';
import HeaderTwo from '../../../components/header/HeaderTwo';
import '../../../style/home.css';

const EvPrime = () => {
  return (
    <>
      <HeaderTwo />
      <div>
        <img className="nexonEv" src="../../../Images/nexonEv.jpg" alt="" />
      </div>
    </>
  );
};

export default EvPrime;
